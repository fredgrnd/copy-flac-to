"""
Ce script scanne les fichiers FLAC à partir d'un répertoire donné, extrait les informations de métadonnées,
vérifie la profondeur de bits, et effectue ensuite une copie ou une conversion selon la profondeur de bits. 
Les métadonnées extraites sont stockées dans une base de données SQLite pour éviter que les fichiers ne soient retravaillés. 
Un paramètre permet de repopuler la base de données à partir des fichiers dans le répertoire cible.
"""

import argparse
import os
import shutil
import sqlite3
import subprocess
from mutagen.flac import FLAC


def get_tags(fichier_path):
    try:
        audio = FLAC(fichier_path)
        artist = audio['artist'][0] if 'artist' in audio else 'Unknown'
        album = audio['album'][0] if 'album' in audio else 'Unknown'
        title = audio['title'][0] if 'title' in audio else 'Unknown'
        tracknumber = audio['tracknumber'][0].split('/')[0] if 'tracknumber' in audio else 'Unknown'
        discnumber = audio['discnumber'][0].split('/')[0] if 'discnumber' in audio else '01'
    except Exception as e:
        print(f"Unable to read tags for file {fichier_path}: {str(e)}")
        artist, album, title, tracknumber, discnumber = 'Unknown', 'Unknown', 'Unknown', 'Unknown', 'Unknown'
    
    return artist, album, title, tracknumber, discnumber


def verifie_bit_depth(fichier_path):
    try:
        audio = FLAC(fichier_path)
        bit_depth = audio.info.bits_per_sample
        return bit_depth
    except Exception as e:
        print(f"Erreur lors de la vérification du bit depth: {str(e)}")
        return None


def traite_fichier(conn, fichier_path, disk_path):
    try:
        curs = conn.cursor()
        bit_depth = verifie_bit_depth(fichier_path)
        if bit_depth:
            artiste, album, title, tracknumber, discnumber = get_tags(fichier_path)
            album = ''.join(c for c in album if c.isalnum() or c.isspace())
            title = ''.join(c for c in title if c.isalnum() or c.isspace())
            curs.execute('SELECT * FROM tags WHERE chemin_fichier = ?', (fichier_path,))
            if curs.fetchone():
                print("Le fichier a déjà été traité.")
                return

            chemin_sortie = os.path.join(disk_path, artiste, album)
            os.makedirs(chemin_sortie, exist_ok=True)
            discnumber = discnumber.zfill(2)
            tracknumber = tracknumber.zfill(2)
            
            nom_fichier = f"{discnumber}-{tracknumber} - {artiste} - {album} - {title}.flac"
            chemin_sortie_fichier = os.path.join(chemin_sortie, nom_fichier)

            if bit_depth == 16:
                shutil.copy2(fichier_path, chemin_sortie_fichier)
            elif bit_depth == 24:
                subprocess.run(['ffmpeg', '-i', fichier_path, '-sample_fmt', 's16', chemin_sortie_fichier], check=True)             
            curs.execute('INSERT INTO tags VALUES (?,?,?,?,?,?)', (artiste, album, title, tracknumber, discnumber, fichier_path))
            conn.commit()

    except Exception as e:
        print(f"Erreur lors du traitement du fichier {fichier_path}: {str(e)}")


def scanne_fichiers_flac_recursif(conn, chemin_dossier, disk_path):
    for racine, dossiers, fichiers in os.walk(chemin_dossier):
        for fichier in fichiers:
            if fichier.endswith('.flac'):
                print(f'Traite fichier: {fichier}')
                fichier_path = os.path.join(racine, fichier)
                traite_fichier(conn, fichier_path, disk_path)


parser = argparse.ArgumentParser(description='Scan FLAC files in a folder.',
                                 epilog='Example of use: python script_name.py /path/to/source_folder /path/to/target_folder')
parser.add_argument('source_folder', help='Path to the folder to scan.')
parser.add_argument('target_folder', help='Path to the output folder.')
parser.add_argument('--repopulate', action='store_true', 
                    help='If set, the database will be repopulated with the files from the target folder.')
args = parser.parse_args()

database_path = os.path.join(args.target_folder, 'tags_db.sqlite')

if args.repopulate and os.path.exists(database_path):
    os.remove(database_path)

conn = sqlite3.connect(database_path)
curs = conn.cursor()

curs.execute('''
    CREATE TABLE IF NOT EXISTS tags (
        artiste TEXT,
        album TEXT,
        title TEXT,
        tracknumber TEXT,
        discnumber TEXT,
        chemin_fichier TEXT
    )
''')

scanne_fichiers_flac_recursif(conn, args.source_folder, args.target_folder)

conn.close()
